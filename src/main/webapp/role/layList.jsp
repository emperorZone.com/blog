<%--
  Created by IntelliJ IDEA.
  User: 84361
  Date: 2020/6/12
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>角色管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body>
    <%--头部多条件查询--%>
    <form class="layui-form" >
        <div class="layui-inline">
            <label class="layui-form-label">角色名</label>
            <div class="layui-input-inline">
                <input type="text" name="rolename" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline">
                <select name="status" >
                    <option value="">请选择</option>
                    <option value="1">正常</option>
                    <option value="0">注销</option>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <div class="layui-input-block">
                <button class="layui-btn" lay-filter="query"  lay-submit>查询</button>
            </div>
        </div>
    </form>
    <table id="roleTable" lay-filter="roleTable"></table>
    <%--html模板，别的地方可以根据ID来引用该模板内容--%>
    <script type="text/html" id="roleToolbar">
        <div class="layui-btn-container"><%--定义一个按钮组--%>
            <button class="layui-btn layui-btn-sm" lay-event="add">新增</button>
            <button class="layui-btn layui-btn-sm" lay-event="delete">删除</button>
        </div>
    </script>
    <script type="text/html" id="innerToolBar">
        <div class="layui-btn-container"><%--定义一个按钮组--%>
            <button class="layui-btn layui-btn-sm" lay-event="edit">修改</button>
            <button class="layui-btn layui-btn-sm" lay-event="delete">删除</button>
        </div>
    </script>
<%--引入js--%>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>

    //layui引入特定模块的方式
    layui.use(['layer', 'form','table','jquery'], function () {
        var layer = layui.layer//layer 弹窗对象
            , form = layui.form;//表单对象
        var table=layui.table;
        var $=layui.$;//引入jquery
        //表单提交事件监听 submit(触发submit的按钮的lay-filter)
        form.on('submit(query)', function(data){
            console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
            console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
            console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
            //重新加载表格
            table.reload("roleTable",{
                where:{//查询条件
                    params:data.field// 会封装到LayRequest中的params中去
                },
                page:{
                    curr:1//当前页
                }
            })


            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });


        table.render({
            elem:"#roleTable",
            url:"${pageContext.request.contextPath}/role/layList",
            toolbar: '#roleToolbar',   //开启头部工具栏
            defaultToolbar: ['filter', 'print', 'exports'],//右侧工具栏
            cols:[[//定义列信息
                {type: 'checkbox', fixed: 'left'},//最左侧复选框
                {field:"id",title:"ID",width:130},//一列数据项
                {field:"rolename",title:"角色名",width:150},
                {field:"explain",title:"描述",width:100},
                {field:"operatorAdmin",title:"操作人",width:130,
                    templet:function (res) {//取对象属性中的属性
                        if(res.operatorAdmin!=null){
                            return res.operatorAdmin.account;
                        }
                        return "";
                    }
                },
                {field:"createtime",title:"创建时间",width:100},
                {field:"status",title:"状态",width:150,
                    templet:function (res) {//使用模板进行显示转换
                        if(res.status=='1')return "正常"
                        return "注销";
                    }},
                {fixed: 'right', title:'操作', toolbar: '#innerToolBar', width:150}//表格内操作栏
            ]],
            page:true
        })
        layer.msg('Hello World');


        //监听头部工具栏事件
        // toolbar(table的lay-filter)
        table.on('toolbar(roleTable)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);//获取到选中的内容
            switch(obj.event){// 工具栏中按钮的 lay-event属性的值
                case 'add':
                    //弹出层
                    layer.open({
                        title:"角色新增",//标题
                        type:2,// 2 为iframe层，可以加载其他的页面到弹出层中
                        content:"${pageContext.request.contextPath}/role/toAdd",
                        area: ['500px', '400px'],//定义宽高
                        maxmin:true//可以最大化
                    })
                    break;
                case 'delete':
                    layer.msg('删除');
                    console.log(checkStatus)
                    console.log(checkStatus.data)//选中的数据
                    if(checkStatus.data.length==0){
                        layer.alert("请至少选择一项删除！");
                        return;
                    }
                    var data=checkStatus.data;
                    var idArray=[];
                    //获取ID值,并放入数组
                    for(var i=0;i<data.length;i++){
                        idArray.push(data[i].id);
                    }
                    console.log(idArray);
                    //执行批量删除
                    $.post("${pageContext.request.contextPath}/role/batchDelete",
                        {ids:idArray.join(",")},function (result) {
                            if(result.code==0){
                                layer.msg("删除成功");
                                //重写加载表格
                                table.reload("roleTable",{
                                    // where:{
                                    //
                                    // },
                                    page:{
                                        curr:1
                                    }
                                })
                                return
                            }
                            layer.msg("删除失败");
                        },"json")
                    break;
            };
        });
        //行内操作栏监听事件

        table.on('tool(roleTable)', function(obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
            switch (layEvent) {
                case 'delete':
                    layer.alert("删除该行记录，id:"+data.id)
                    //todo 发ajax请求异步删除数据
                    break;
                case 'edit':
                    layer.open({
                        title:"角色修改",//标题
                        type:2,// 2 为iframe层，可以加载其他的页面到弹出层中
                        content:"${pageContext.request.contextPath}/role/toUpdate?id="+data.id,
                        area: ['500px', '400px'],//定义宽高
                        maxmin:true//可以最大化
                    })
                    break;
            }

            })
        });
</script>
</body>
</html>
