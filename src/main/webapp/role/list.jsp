<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: 84361
  Date: 2020/6/8
  Time: 15:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>角色列表页面</title>
</head>
<body>
<%--点击按钮跳转到后台方法，并转发到新增页面--%>
<a href="${pageContext.request.contextPath}/role/toAdd">新增</a>
<table>
    <thead>
        <tr>
            <td>ID</td>
            <td>角色名</td>
            <td>描述</td>
            <td>状态</td>
            <td>操作人</td>
            <td>创建时间</td>
            <td>操作</td>
        </tr>
    </thead>
    <tbody>
    <c:forEach items="${roles}" var="role">
        <tr>
            <td>${role.id}</td>
            <td>${role.rolename}</td>
            <td>${role.explain}</td>
            <td>${role.status=="1"?"正常":"注销"}</td>
            <td>${role.operator}</td>
            <td><fmt:formatDate value="${role.createtime}" pattern="yyyy-MM-dd"></fmt:formatDate></td>
            <td>
                <a href="${pageContext.request.contextPath}/role/delete?id=${role.id}">删除</a>
                <%--点击跳转到后台查询用户信息的控制层方法，之后再跳转到修改页面--%>
                <a href="${pageContext.request.contextPath}/role/toUpdate?id=${role.id}">修改</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
