<%--
  Created by IntelliJ IDEA.
  User: 84361
  Date: 2020/6/8
  Time: 16:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>角色新增</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body>
<%--layui-form  layui的表单样式，添加到form表单上--%>
<form class="layui-form" >
    <%--一个表单项--%>
    <div class="layui-form-item">
        <label class="layui-form-label">角色名</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <input type="text" name="rolename" lay-verify="required" placeholder="请输入角色名" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <input type="radio" name="status" value="1" checked title="正常">
            <input type="radio" name="status" value="0" title="注销">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">操作人</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <%--lay-verify="number" 必须是数字--%>
            <input type="text" name="operator" lay-verify="number" value="${sessionScope.admin.id}" placeholder="操作人" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">描述</label>
        <div class="layui-input-inline">
            <textarea type="text" name="explain" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item" >
        <%--lay-submit 标识该按钮可以提交表单--%>
        <input type="button" style="margin-left: 100px" lay-filter="add" lay-submit class="layui-btn" value="提交">
    </div>
</form>
<%--引入js--%>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    //layui引入特定模块的方式
    layui.use(['layer', 'form','jquery'], function () {
        var layer = layui.layer//layer 弹窗对象
            , form = layui.form;//表单对象
        var $=layui.$;

        layer.msg('Hello World');
        //表单提交监听
        form.on("submit(add)",function (data) {
            //发起异步请求，新增 data.field 表单数据
            $.post("/role/add2",data.field,function (result) {
                    if(result.code==0){
                        layer.msg("新增成功")
                        //刷新父窗口的表格 parent 父窗口对象
                        parent.layui.table.reload("roleTable",{
                            page:{
                                curr:1
                            }
                        })
                        //关闭本窗口
                        //当你在iframe页面关闭自身时
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                    }
                    // else{
                    //     layer.alert(result.msg);
                    // }
                 else if(result.code==403){
                     window.location.href="${pageContext.request.contextPath}/403.jsp"
                    }

            },"json")
            return false;
        })
    });
</script>
</body>
</html>
