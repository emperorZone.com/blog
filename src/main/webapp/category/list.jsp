<%--
  Created by IntelliJ IDEA.
  User: 84361
  Date: 2020/6/11
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>演示二级下拉联动</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">

</head>
<body>
<form class="layui-form">
    <%--一个表单项--%>
    <div class="layui-form-item">
        <label class="layui-form-label">一级菜单</label>
        <div class="layui-input-inline">
            <%--lay-filter="one" 相当于layui中的ID--%>
            <select id="one" lay-filter="one">

            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">二级菜单</label>
        <div class="layui-input-inline">
            <select id="two">

            </select>
        </div>
    </div>
</form>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    //layui引入特定模块的方式
    layui.use(['layer', 'form','jquery'], function () {
        var layer = layui.layer//layer 弹窗对象
            , form = layui.form;//表单对象
        var $=layui.$;//引入jquery
        layer.msg('Hello World');
        //发异步请求获取一级菜单
        $.get("${pageContext.request.contextPath}/category/childCategory",{parentid:-1},function(result){
            if(result.code==200){
                var html="<option value=''>请选择</option>";
                //获取数据，并展示到下拉列表
                for(var i=0;i<result.data.length;i++){
                    html+="<option value='"+result.data[i].id+"'>"+result.data[i].name+"</option>"
                }
                //将内容添加到一级菜单下拉列表
                $("#one").html(html);
                // 修改了表单元素之后，需要重新渲染
                form.render(); //更新全部
            }
        },"json")

        //select 框监听事件
        form.on('select(one)', function(data){
            if(data.value==''){//如果是请选择，则将二级菜单 清空
                $("#two").empty();
                form.render();
                return;
            }
            $.get("${pageContext.request.contextPath}/category/childCategory",{parentid:data.value},function(result){
                if(result.code==200){
                    var html="<option value=''>请选择</option>";
                    //获取数据，并展示到下拉列表
                    for(var i=0;i<result.data.length;i++){
                        html+="<option value='"+result.data[i].id+"'>"+result.data[i].name+"</option>"
                    }
                    //将内容添加到一级菜单下拉列表
                    $("#two").html(html);
                    // 修改了表单元素之后，需要重新渲染
                    form.render(); //更新全部
                }
            },"json")
        });

    });
</script>
</body>
</html>
