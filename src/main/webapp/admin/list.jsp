<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: 84361
  Date: 2020/6/9
  Time: 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理员列表</title>
</head>
<body>
<%--多条件查询--%>
<form action="${pageContext.request.contextPath}/admin/list" method="post">
    账号：<input type="text" name="account" value="${params.account}">
    状态：<select name="status">
    <option value="">请选择</option>
    <option value="1" <c:if test="${params.status=='1'}"> selected</c:if> >正常</option>
    <option value="0" <c:if test="${params.status=='0'}"> selected</c:if>  >注销</option>
</select>
    姓名：<input type="text" name="name" value="${params.name}">
    <%--分页查询--%>
    <input type="hidden" name="pageNum" value="1">
    <input type="hidden" name="pageSize" value="1">
    <input type="submit" value="查询">
</form>
<a href="${pageContext.request.contextPath}/admin/toAdd">新增</a>
<table>
    <thead>
    <tr>
        <td>ID</td>
        <td>账号</td>
        <td>用户名</td>
        <td>性别</td>
        <td>状态</td>
        <td>角色</td>
        <td>手机</td>
        <td>邮箱</td>
        <td>头像</td>
        <td>操作人</td>
        <td>操作</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${page.list}" var="admin">
        <tr>
            <td>${admin.id}</td>
            <td>${admin.account}</td>
            <td>${admin.name}</td>
            <td>${admin.sex=="1"?"男":"女"}</td>
            <td>${admin.status=="0"?"注销":"正常"}</td>
            <td>${admin.role.rolename}</td>
            <td>${admin.phone}</td>
            <td>${admin.email}</td>
            <td>
                <img style="width: 25px;height: 25px;border-radius: 50%;"
                     src="${pageContext.request.contextPath}${admin.headpic}">
                <a href="${pageContext.request.contextPath}/admin/headpicDownload?filename=${fn:replace(admin.headpic,"/head/" ,"")}">下载</a>
            </td>
            <td>${admin.operatorAdmin.account}</td>
            <td>
                <a href="">修改</a>&nbsp;&nbsp;
                <a href="javascript:deleteAdmin(${admin.id})">删除</a>
                <a href="">修改密码</a>
                <a href="${pageContext.request.contextPath}/admin/headpic.jsp?id=${admin.id}">修改头像</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div>
    <a href="javascript:goPage(${page.pageNum-1})">上一页</a>
    <a href="javascript:goPage(${page.pageNum+1})">下一页</a>
    当前页：${page.pageNum},总记录数：${page.total},总页数：${page.pages}
</div>
<script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.min.js"></script>
<script>
    //跳转对应页面
    function goPage(num) {
        $("input[name='pageNum']").val(num);
        //提交表单
        $("form").submit();
    }

    function deleteAdmin(id) {
        if (confirm("确认要删除吗？")) {
            //异步删除,通过jquery发送ajax请求
            $.ajax({
                url: "${pageContext.request.contextPath}/admin/delete",
                type: "get",//请求方式
                data: {"id": id},
                dataType: "json",
                success: function (result) {
                    if (result.code == 200) {
                        alert("删除成功！")
                        //重新加载页面或跳转页面
                        window.location.reload();
                    } else {
                        alert("删除失败！")
                    }
                },
                error: function () {
                    alert("删除失败！")
                }
            })
        }
    }
</script>
</body>
</html>
