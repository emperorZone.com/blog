<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 84361
  Date: 2020/6/9
  Time: 10:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理员新增</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">

</head>
<body>
<%----%>
<form class="layui-form" action="${pageContext.request.contextPath}/admin/add" method="post">
    <div class="layui-form-item">
        <label class="layui-form-label">账号</label>
        <div class="layui-input-inline">
            <input type="text" name="account" lay-verify="required" class="layui-input" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-inline">
            <input type="password" name="password" lay-verify="required" class="layui-input" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input type="text" name="name" lay-verify="required" class="layui-input" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">手机</label>
        <div class="layui-input-inline">
            <input type="text" name="phone" lay-verify="phone" class="layui-input" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input type="text" name="email" lay-verify="email" class="layui-input" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-inline">
            <input type="radio" name="status" checked value="1"  title="正常">
            <input type="radio" name="status"  value="0" title="注销" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-inline">
            <select name="sex">
                <option value="1">男</option>
                <option value="2">女</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">角色</label>
        <div class="layui-input-inline">
            <select name="roleid">
                <c:forEach items="${roles}" var="role">
                    <option value="${role.id}">${role.rolename}</option>
                </c:forEach>
            </select>
        </div>
    </div>
     <input type="hidden" name="operator" value="${sessionScope.admin.id}">
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="submit" lay-submit class="layui-btn" value="提交">
        </div>
    </div>
</form>

<%--引入js--%>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>

    //layui引入特定模块的方式
    layui.use(['layer', 'form'], function () {
        var layer = layui.layer//layer 弹窗对象
            , form = layui.form;//表单对象

        layer.msg('Hello World');
    });
</script>
</body>
</html>
