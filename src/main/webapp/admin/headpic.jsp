<%--
  Created by IntelliJ IDEA.
  User: 84361
  Date: 2020/6/10
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>头像上传</title>
</head>
<body>
<%--
    文件上传
    1）method 必须指定为post
    2) enctype 必须指定为multipart/form-data
--%>
<form action="/admin/headpic" method="post" enctype="multipart/form-data">
    选择头像 <input type="file" name="headpic">
    <%--${param.属性值}==request.getParameter(属性值)--%>
    <input type="text" name="id" value="${param.id}">
    <input type="submit" value="上传">
</form>
</body>
</html>
