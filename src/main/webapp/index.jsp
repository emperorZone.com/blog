<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>layout 后台大布局 - Layui</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">在线考试系统后台</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">控制台</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">用户管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="">系统用户</a></dd>
                    <dd><a href="">注册用户</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    贤心
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="">退了</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <%--遍历菜单--%>
                <c:forEach var="menu" items="${sessionScope.role.resourceList}">
                    <li class="layui-nav-item">
                        <c:if test="${menu.parentid==-1}">
                            <c:if test="${not empty menu.url}"><%--一级菜单，没有子菜单的情况--%>
                                <a target="middle" href="${pageContext.request.contextPath}${menu.url}">${menu.name}</a>
                            </c:if>
                            <%--展示二级目录--%>
                            <c:if test="${empty menu.url}"><%--显示带子菜单的一级菜单--%>
                                <a  href="javascript:;">${menu.name}</a>
                                <dl class="layui-nav-child">
                                    <c:forEach var="childMenu" items="${sessionScope.role.resourceList}">
                                        <c:if test="${childMenu.parentid==menu.id}"><%--如果菜单的父菜单为本菜单，则显示在当前菜单下--%>
                                            <dd><a target="middle" href="${pageContext.request.contextPath}${childMenu.url}">${childMenu.name}</a></dd>
                                        </c:if>
                                    </c:forEach>
                                </dl>
                            </c:if>
                        </c:if>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>

    <div class="layui-body" >
        <!-- 内容主体区域 -->
        <%--通过iframe 加载功能页面--%>
        <iframe height="100%" width="100%" id="middle" name="middle"
                src="${pageContext.request.contextPath}/admin/layList.jsp" frameborder="0">
        </iframe>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use('element', function(){
        var element = layui.element;

    });
</script>
</body>
</html>