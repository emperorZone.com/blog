package entity;

/**
 * @author bigpeng
 * @create 2020-06-11-9:54
 */
public class ThreadTest {
    protected int al;
    protected final int a2=1;
    public void test(int il,final int i2){
        final int i3=100;
        switch (il){
            case i3:
                System.out.println("a");
        }
    }
    public static void main(String[] args) {
        ThreadTest threadTest = new ThreadTest();
        User user = new User("james",1);
        Task task = threadTest.new Task(user);
        for (int i = 0; i < 10; i++) {
            new Thread(threadTest.new Task(user)).start();
        }
        try {
            Thread.interrupted();//中断当前线程，会将中断标识设为true
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(new Integer(1).equals(new Long(1)));
    }
    //synchronized(this.getClass){}
    public static synchronized void test(){

    }

    class Task implements Runnable{
        private User user;

        public Task(User user) {
            this.user = user;
        }

        @Override
        public void run() {
            //当线程共享锁对象时，会是同步的
            synchronized (user){
                user.setAge(user.getAge()+1);
                System.out.println("线程"+Thread.currentThread().getName()+"执行");
                try {
                    user.notifyAll();
                    user.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
