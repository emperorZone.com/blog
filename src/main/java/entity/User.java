package entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author bigpeng
 * @create 2020-06-10-9:59
 */
public class User  implements Comparable<User>{
    private String name;
    private Integer age;


    public static void main(String[] args) {
        List<User> users=new ArrayList<>();

        users.add(new User("yao",19));
        users.add(new User("zhang",20));
        users.add(new User("li",17));
        users.add(new User("xu",15));
        users.add(new User("xupeng",15));

//        users.sort(new UserComparator());
        //需要传入的集合中存储的类型实现了Comparable接口
        Collections.sort(users);
//        Collections.sort(users,new UserComparator());


        System.out.println(users);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 当前对象 this与传入的其他对应的比较方法
     * @param o
     * @return
     */
    @Override
    public int compareTo(User o) {
        int age=this.getAge()- o.getAge();
        return age!=0?age:this.getName().length()-o.getName().length();
    }
}
