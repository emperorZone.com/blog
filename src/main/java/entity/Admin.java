package entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author bigpeng
 * @create 2020-06-09-10:36
 */
public class Admin implements Serializable {
    private Integer id;
    private String account;
    private String password;
    private String name;
    private String phone;
    private String email;
    private String status;
    private String sex;
    private String headpic;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")//配置日期类型转换 成json时的格式
    private Date createtime;
    private Date updatetime;
    //todo 关联查询 操作人
    private Integer operator;
    //操作管理员
    private Admin operatorAdmin;
    private Integer roleid;
    // todo 关联查询角色信息
    private Role role;

    public Admin(Integer id) {
        this.id=id;
    }

    public Admin() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHeadpic() {
        return headpic;
    }

    public void setHeadpic(String headpic) {
        this.headpic = headpic;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Admin getOperatorAdmin() {
        return operatorAdmin;
    }

    public void setOperatorAdmin(Admin operatorAdmin) {
        this.operatorAdmin = operatorAdmin;
    }
}
