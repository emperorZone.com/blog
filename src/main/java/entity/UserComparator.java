package entity;

import java.util.Comparator;

/**
 * 实现比较器接口，并重写compare方法
 * @author bigpeng
 * @create 2020-06-10-10:03
 */
public class UserComparator implements Comparator<User> {
    /**
     * 定义比较规则
     *  0 相等  正数 o1>o2  负数 o1<o2
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(User o1, User o2) {
        int age=o1.getAge()- o2.getAge();
        return age!=0?age:o1.getName().length()-o2.getName().length();
    }
}
