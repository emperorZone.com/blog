package entity;

/**
 * @author bigpeng
 * @create 2020-06-09-11:35
 */
public class PageParam {
    private int pageSize;
    private int pageNum;
    private String name;
}
