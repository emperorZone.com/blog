package entity;

/**
 *  定义通用异步请求返回对象
 * @author bigpeng
 * @create 2020-06-09-15:46
 */
public class MyResult<T> {
    //状态码  200 成功 500 失败
    private int code;
    //返回的提示消息
    private String msg;
    // 总记录条数
    private long count;
    //返回的数据
    private  T Data;

    public MyResult() {
    }

    public MyResult(int code, long count, T data) {
        this.code = code;
        this.count = count;
        Data = data;
    }

    public MyResult(int code) {
        this.code = code;
    }

    public MyResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public MyResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        Data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
