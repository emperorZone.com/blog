package controller;

import entity.Article;
import entity.MyResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import service.ArticleService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author bigpeng
 * @create 2020-06-15-16:52
 */
@Controller
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @ResponseBody
    @PostMapping("/uploadImg")
    public MyResult<Map> uploadImg(MultipartFile file, HttpServletRequest request) throws IOException {

        String filename = file.getOriginalFilename();
        System.out.println("上传的文件名："+filename);
        File f=new File("E:/articleImg/"+filename);
        if(!f.getParentFile().exists()){
            f.getParentFile().mkdirs();//如果父目录不存在，创建该目录
        }
        //保存文件,将上传的文件内容写入file
        file.transferTo(f);
        Map map=new HashMap();
        map.put("src",request.getContextPath()+"/articleImg/"+filename);//图片访问路径
        map.put("title",filename);//图片标题
        return new MyResult<>(0,"",map);
    }

    @ResponseBody
    @PostMapping("/article/save")
    public MyResult<String> save(Article article){
        articleService.insert(article);
        return new MyResult<>(0);
    }
}
