package service;

import com.github.pagehelper.PageInfo;
import entity.Admin;
import entity.LayRequest;

import java.util.Map;

/**
 * @author bigpeng
 * @create 2020-06-09-11:02
 */
public interface AdminService {

    boolean insert(Admin admin);
    Admin  selectById(Integer id);
    Admin selectByAccount(String account);

    /**
     * 分页查询
     * @param map 查询条件
     * @return
     */
    PageInfo<Admin> selectByPage(Map<String,Object> map);

    boolean delete(Integer id);
    boolean update(Admin admin);

    boolean updateHeadPic(Admin admin);

    PageInfo<Admin> selectByPage(LayRequest layRequest);

}
