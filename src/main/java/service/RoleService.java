package service;

import com.github.pagehelper.PageInfo;
import entity.LayRequest;
import entity.Role;

import java.util.List;

/**
 * @author bigpeng
 * @create 2020-06-08-15:26
 */
public interface RoleService {

    boolean insert(Role role);

    List<Role> selectAll();

    boolean delete(Integer id);

    Role selectById(Integer id);

    boolean update(Role role);

    PageInfo<Role> selectByPage(LayRequest layRequest);

    int batchDelete(Integer[] ids);

}
