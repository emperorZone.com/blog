package service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import entity.LayRequest;
import entity.Role;
import mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author bigpeng
 * @create 2020-06-08-15:28
 */
@Service//service层注解
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public boolean insert(Role role) {
        return roleMapper.insert(role)>0?true:false;
    }

    @Override
    public List<Role> selectAll() {
        return roleMapper.selectAll();
    }

    @Override
    public boolean delete(Integer id) {
        return roleMapper.delete(id)>0?true:false;
    }

    @Override
    public Role selectById(Integer id) {
        return roleMapper.selectById(id);
    }

    @Override
    public boolean update(Role role) {
        //设置修改时间
        role.setUpdatetime(new Date());
        return roleMapper.update(role)>0?true:false;
    }

    @Override
    public PageInfo<Role> selectByPage(LayRequest layRequest) {
        //1.设置当前页和每页记录数
        PageHelper.startPage(layRequest.getPage(),layRequest.getLimit());
        //2. 执行查询
        List<Role> roles=roleMapper.selectByMap(layRequest.getParams());
        //3.创建PageInfo对象
        return new PageInfo<>(roles);
    }

    @Override
    public int batchDelete(Integer[] ids) {
        return roleMapper.batchDelete(ids);
    }
}
