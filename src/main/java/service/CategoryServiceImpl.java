package service;

import entity.Category;
import mapper.CategoryMapper;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author bigpeng
 * @create 2020-06-11-14:27
 */
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public List<Category> selectByParentId(Integer parentId) {
        return categoryMapper.selectByParentId(parentId);
    }
}
