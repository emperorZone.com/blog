package service;

import entity.Category;

import java.util.List;

/**
 * @author bigpeng
 * @create 2020-06-11-14:26
 */
public interface CategoryService {

    List<Category> selectByParentId(Integer parentId);
}
