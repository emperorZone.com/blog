package service;

import entity.Article;
import mapper.ArticleMapper;
import org.aspectj.lang.annotation.AfterReturning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author bigpeng
 * @create 2020-06-16-10:04
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    @Override
    public boolean insert(Article article) {
        return articleMapper.insert(article)>0?true:false;
    }
}
