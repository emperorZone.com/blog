package service;

import entity.Article;

/**
 * @author bigpeng
 * @create 2020-06-16-10:04
 */
public interface ArticleService {

    boolean insert(Article article);
}
