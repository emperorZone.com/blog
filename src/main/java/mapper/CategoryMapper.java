package mapper;

import entity.Category;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author bigpeng
 * @create 2020-06-11-14:20
 */
public interface CategoryMapper  {
    @Select("select * from t_category where parentid=#{parentId}")
    List<Category> selectByParentId(Integer parentId);
}
