package mapper;

import entity.Admin;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * @author bigpeng
 * @create 2020-06-09-10:43
 */
public interface AdminMapper {

    int insert(Admin admin);

    @Select("select * from t_admin")
    List<Admin> selectAll();

    @Select("select * from t_admin where account=#{account}")
    Admin selectByAccount(String account);

    @Select("select * from t_admin where id=#{id}")
    Admin selectById(Integer id);
    @Delete("delete from t_admin where id=#{id}")
    int delete(Integer id);

    int update(Admin admin);

    List<Admin> selectByMap(Map<String, Object> map);

    List<Admin> selectWithRole(Map<String,Object> map);

    @Update("update t_admin set headpic=#{headpic} where id=#{id}")
    int updateHeadPic(Admin admin);
}
