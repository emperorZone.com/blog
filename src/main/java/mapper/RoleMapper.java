package mapper;

import entity.Resource;
import entity.Role;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author bigpeng
 * @create 2020-06-08-14:58
 */
@Repository//dao层主机
public interface RoleMapper {
    /**
     * 插入角色
     * @param role
     * @return
     */
    int insert(Role role);

    /**
     * 查询所有角色
     * @return
     */
    List<Role> selectAll();

    int delete(Integer id);

    Role selectById(Integer id);

    int update(Role role);
    //根据角色ID查询所有权限
    List<Resource> selectByRoleId(Integer roleid);

    List<Role> selectByMap(Map<String, Object> params);

    int batchDelete(Integer[] ids);
}
