package mapper;

import entity.Article;

/**
 * @author bigpeng
 * @create 2020-06-16-10:01
 */
public interface ArticleMapper {

    int insert(Article article);
}
